
// Assignment 4 - Salary
// Devon Lozier

#include <iostream>
#include <conio.h>

using namespace std;

/// <summary>
/// The goal is to insert this structure data into an array, 
/// which should be looped through in the console and return
/// employee information, including gross pay per employee.
/// </summary>
/// <returns>Employee ID, Full Name, Gross Pay, TOTAL Gross Pay</returns>
struct Employee {
	int ID = 0;
	string FirstName;
	string LastName;
	float PayRate = 0;
	float Hours = 0;
};

//Employee* CreateEmployee(int ID, string FN, string LN, float pay, float hours) // Factory function.
//{
//	// Employee e = Employee{ID, FN, LN, pay, hours}; // Instantiate struct on stack
//	// return &e
//	
//	Employee *pE = new Employee{ID, FN, LN, pay, hours}; // Must delete later.
//
//	return pE;
//
//	//pE->ID = ID; // trying to take the stuff from the factory function instead.
//	//pE->FirstName = FN;
//	//(*pE).LastName = LN;	
//}

void PrintEmployeeInfo(const Employee *pEmployee)
{
	cout << "\nPrinting employee information... \n \n";
	cout << "Employee " << pEmployee->ID << ": \n";
	cout << "Name: " << pEmployee->LastName << ", " << pEmployee->FirstName << "\n";
	cout << "Gross Pay: " << (pEmployee->PayRate * pEmployee->Hours) << "\n \n";
}

void PrintEmployees(Employee *pEmployees, int size)
{
	float totalGrossPay = 0;

	for (int i = 0; i < size; i++)
	{
		//PrintEmployeeInfo(pEmployees + i); //take full space in memory of struct Employee and
											// move down to the next full space in memory of struct Employee
		PrintEmployeeInfo(&pEmployees[i]);

		totalGrossPay += pEmployees[i].PayRate * pEmployees[i].Hours;
	}
	cout << "\nTotal Gross Pay: " << totalGrossPay << "\n";
}

void EmployeesArrayOnHeap()
{
	int size = 0;
	float totalGrossPay = 0;

	cout << "Enter the number of employees: ";
	cin >> size;

	Employee *pEmployees = new Employee[size]; // array on the heap.

	for (int i = 0; i < size; i++)
	{
		cout << "Employee " << (i + 1) << " ID: ";
		cin >> pEmployees[i].ID;

		cout << "Employee " << (i + 1) << " First Name: ";
		cin >> pEmployees[i].FirstName;

		cout << "Employee " << (i + 1) << " Last Name: ";
		cin >> pEmployees[i].LastName;

		cout << "Employee " << (i + 1) << " Hourly Wage: ";
		cin >> pEmployees[i].PayRate;

		cout << "Employee " << (i + 1) << " Hours Worked: ";
		cin >> pEmployees[i].Hours;

		cout << "\n";
	}

	//cout << "\n" <<  "Employees List: " << *pEmployees;
	
	PrintEmployees(pEmployees, size);


	delete[] pEmployees; // each thing created must be deleted to avoid memory leaks.
}


int main()
{
	EmployeesArrayOnHeap();

	//const int NUM_EMPLOYEES = 5;
	//Employee employees[NUM_EMPLOYEES];

	//float totalGrossPay = 0;

	//for (int i = 0; i < NUM_EMPLOYEES; i++)
	//{
	//	cout << "Enter employee: \n";

	//	cout << "ID: ";
	//	cin >> employees[i].ID;

	//	cout << "First Name: ";
	//	cin >> employees[i].FirstName;

	//	cout << "Last Name: ";
	//	cin >> employees[i].LastName;

	//	cout << "Hourly Wage: ";
	//	cin >> employees[i].PayRate;

	//	cout << "Hours Worked: ";
	//	cin >> employees[i].Hours;

	//	PrintEmployeeInfo(&employees[i]);
	//	

	//	totalGrossPay += employees[i].PayRate * employees[i].Hours;
	//}

	//cout << "Total Gross Pay: " << totalGrossPay << "\n\nPress any key to exit.";

	(void)_getch();
	return 0;
}
